#!/usr/bin/env bash
#Written by Clément Martinache
# 06 Jul 2021

TENTATIVES_MAX=10
BLEU="\e[34m1\e[0m"
ROUGE="\e[31m2\e[0m"
VERT="\e[32m3\e[0m"
JAUNE="\e[33m4\e[0m"
CYAN="\e[96m5\e[0m"
MAGENTA="\e[35m6\e[0m"

COULEURS=( $BLEU $ROUGE $VERT $JAUNE $CYAN $MAGENTA)
COULEURS_LETTRES=( "1" "2" "3" "4" "5" "6" )

LANG="x"
	while [[ $LANG != "en" &&  $LANG != "fr" && $LANG != "es" ]] 
	do
		read -p $'Enter en for english / Entrez fr pour le français / Ingrese es para español\n' LANG
	done



source lang_$LANG
#Fonction pour vérifier si un array contient une valeur,
#Le premier argument est l'array, le deuxième l'élément
#https://stackoverflow.com/questions/14366390/check-if-an-element-is-present-in-a-bash-array
array_contains () { 
    local array="$1[@]"
    local seeking=$2
    local in=1
    for element in "${!array}"; do
        if [[ $element == "$seeking" ]]; then
	    in=0
            break
fi
done
    return $in
}


#Repète un caractère n fois. 
#Premier argument le caractère, deuxième argument le nombre de fois
#TODO: replace with seq
repeat(){
	if [ $2 -gt 0 ]; then
		for i in $(eval echo "{1..$2}"); do echo -en "$1"; done
	fi
}

#Fonction pour vérifier si l'utilisateur rentre des couleurs valides
#Demande 4 arguments (pas de test attention !)
check_couleurs () {
	arr=( "$1" "$2" "$3" "$4" )
	echo ${arr[@]}
	
	for e in "${arr[@]}"
	do
		if array_contains COULEURS_LETTRES $e; then
			true
		else
		       return 1
		fi
	done
	return 0
}

#Fonction pour demander à l'utilisateur sa réponse
entree_utilisateur () {
	NOT_OK=true
	while $NOT_OK ; do	
	read ENTREE_UTILISATEUR ;	
		#On passe tout en majuscules
		REPONSE_UTILISATEUR_RAW=($ENTREE_UTILISATEUR)
		REPONSE_UTILISATEUR=()
		for ELEMENT in "${REPONSE_UTILISATEUR_RAW[@]}" 
		do
			REPONSE_UTILISATEUR+=("${ELEMENT^^}")
		done
		
		#On vérifie d'abord qu'il y'a bien #4 valeurs
		if [[ "${#REPONSE_UTILISATEUR[@]}" -ne 4 ]] ; then 
			echo $MSG_MAUVAIS_NOMBRE_VALEURS >&2
		#On vérifie que ce qui a été entré appartient à COULEURS
		elif ! check_couleurs ${REPONSE_UTILISATEUR[@]} ; then
			echo $MSG_COULEURS_INEXISTANTES >&2
		else
			NOT_OK=false
		fi
	done
	echo "${REPONSE_UTILISATEUR[@]}" 	
}	

#Fonction pour mettre le résultat en couleur
#Pas de vérif mais ce sera toujours appelé sur quelque chose de valide
mettre_couleurs() {
	case "${1}" in
    		1)     echo -e $BLEU ;;
    		2)     echo -e $ROUGE ;;
    		3)     echo -e $VERT ;;
    		4)     echo -e $JAUNE ;;
    		5)     echo -e $CYAN ;;
    		6)     echo -e $MAGENTA ;;
esac
}

#verifier_reponse prend la vraie réponse comme premier argument,
#la réponse de l'utilisateur comme deuxième
#Retourne 1 si la réponse est mauvaise, 0 sinon 
#TODO:
#Je n'ai pas réussi à passer des arrays comme arguments,
#du coup j'ai passé tous les éléments des arrays et 
#je les reconstruis derrière.
verifier_reponse() {
	REP=("$1" "$2" "$3" "$4") #La vraie réponse
	REP_UTIL=("$5" "$6" "$7" "$8") #La réponse de l'utilisateur
	#TODO: Moche
	REP_UTIL_COULEURS=("$(mettre_couleurs $5)" "$(mettre_couleurs $6)" "$(mettre_couleurs $7)" "$(mettre_couleurs $8)")
	#On commence par vérifier si la réponse de l'utilisateur est la bonne,
	#Auquel cas c'est trivial
        #NOTE: On pourrait s'épargner ce test, et simplement vérifier
	#si $BIEN_PLACES == 4 à la fin de la boucle
	#J'ai choisi de faire comme ça pour éviter de 
	# passer par la boucle et de faire plein de tests
	#inutiles pour une question de performance, même si en l'occurence
	#on ne gagne pas grand chose...
	A=${REP[@]};
	B=${REP_UTIL[@]};
	if [ "$A" == "$B" ] ; then
		echo -en "\033[1A\033[2K" 
		echo -n "${REP_UTIL_COULEURS[@]} | "
		repeat "\e[90mN\e[0m " 4 
		echo -e "\n"
		return 0;
	else
	#Sinon on vérifie élément par élément
	#En faisant attention aux éléments déjà trouvés
		IDX=0
		BIEN_PLACES=0
		BONNES_COULEURS=0
		MAUVAIS=0
		for ELEMENT in "${REP[@]}"
		do
			if [ "$ELEMENT" == "${REP_UTIL[$IDX]}" ]; then
				((BIEN_PLACES++))
				unset 'REP_UTIL[$IDX]'
			elif array_contains REP_UTIL $ELEMENT; then
				((BONNES_COULEURS++))
			else
				((MAUVAIS++))
			fi
			((IDX++))	
		done
				
		echo -en "\033[1A\033[2K" 
		echo -n "${REP_UTIL_COULEURS[@]} | "
		repeat "\e[90mN\e[0m " $BIEN_PLACES
		repeat "\e[97mB\e[0m " $BONNES_COULEURS
		repeat "\e[37mX\e[0m " $MAUVAIS
		echo ""
		return 1
	fi
}

#Affichage de bienvenue
echo "

  __  __    _    ____ _____ _____ ____  __  __ ___ _   _ ____  
 |  \/  |  / \  / ___|_   _| ____|  _ \|  \/  |_ _| \ | |  _ \ 
 | |\/| | / _ \ \___ \ | | |  _| | |_) | |\/| || ||  \| | | | |
 | |  | |/ ___ \ ___) || | | |___|  _ <| |  | || || |\  | |_| |
 |_|  |_/_/   \_\____/ |_| |_____|_| \_\_|  |_|___|_| \_|____/ 
                                                               
"                             
echo -e $MSG_BIENVENUE

cajoue="1"
while [[ $cajoue == "1" ]] 
do
	TENTATIVES=0
	#Générer la réponse : 6 nombres aléatoires correspondant aux couleurs
	REPONSE=( )
	REP_COULEURS=( )
	while [[ ${#REPONSE[@]} -lt 4 ]]
	do 
		IDX=$(( $RANDOM % 6 ))
		REPONSE+=( ${COULEURS_LETTRES[$IDX]} )
		REP_COULEURS+=( $(mettre_couleurs ${COULEURS_LETTRES[$IDX]})  )
	done
	#Pour les tests
	#echo "La réponse est ${REPONSE[@]}"
	
	#Début du jeu
	gagne=0
	while [[ $TENTATIVES -lt $TENTATIVES_MAX ]]
	do
		((TENTATIVES++))
		REPONSE_UTILISATEUR=($(entree_utilisateur))
		#La vérifier avec la vraie réponse
		if verifier_reponse "${REPONSE[@]}" "${REPONSE_UTILISATEUR[@]}" ;
		then 
			echo $MSG_VICTOIRE $TENTATIVES $MSG_VICTOIRE_FIN
			gagne=1
			break
		fi
	done

	if [[ $TENTATIVES == $TENTATIVES_MAX ]] && [[ $gagne == "0" ]]; then
		echo -e $MSG_DEFAITE ${REP_COULEURS[@]}
	fi
	
	REJOUER="x"
	while [[ $REJOUER != $OUI &&  $REJOUER != $NON ]] 
	do
		read -p "$MSG_DEMANDE_REJOUER" REJOUER
		if [ -z $REJOUER ] || [ ${REJOUER^^} == $OUI ];
		then
			REJOUER=$OUI
			echo $MSG_REJOUE
		elif [[ ${REJOUER^^} == $NON ]]; then
			REJOUER=$NON
			cajoue="0"
			echo $MSG_FIN
		else
			echo $MSG_ERR_REJOUER
		fi
	done
done
